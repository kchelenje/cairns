import 'package:Libresmit/ui/al_books.dart';
import 'package:Libresmit/ui/home.dart';
import 'package:flutter/material.dart';

class Landing extends StatefulWidget {
  final String contentLink;

  Landing(this.contentLink);

  @override
  _LandingState createState() =>
      new _LandingState(dataLink: new Landing(contentLink));
}

class _LandingState extends State<Landing> with SingleTickerProviderStateMixin {
  @override
  void initState() {
    super.initState();
  }

  final Landing dataLink;
  _LandingState({Key key, @required this.dataLink});

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text(
            'Cairns App',
            style: new TextStyle(fontFamily: 'mse-app-bars', fontSize: 18.0),
          ),
          backgroundColor: Colors.green[900],
          centerTitle: true,
        ),
        body: new Container(
            decoration: BoxDecoration(
                image: new DecorationImage(
                    fit: BoxFit.cover,
                    colorFilter: new ColorFilter.mode(
                        Colors.black.withOpacity(0.7), BlendMode.dstATop),
                    image: new AssetImage("images/background.jpg"))),
            child: new FutureBuilder(
                future: DefaultAssetBundle.of(context)
                    .loadString('${dataLink.contentLink}'),
                builder: (context, snapshot) {
                  //  var topicData = json.decode(snapshot.data.toString());
                  return new Container(
                      padding: EdgeInsets.only(top: 20.0),
                      child: ListView(children: <Widget>[
                        new Column(
                          children: <Widget>[application()],
                        ),
                      ]));
                })));
  }

  Widget application() {
    return new Column(
      children: <Widget>[
        Container(
            margin: EdgeInsets.only(bottom: 20.0, top: 25.0),
            child: new Text(
              "Welcome to Cairns Agent Portal",
              style: TextStyle(
                  fontSize: 25.0,
                  fontWeight: FontWeight.w900,
                  fontFamily: "mse-app-bars",
                  color: Colors.green[900]),
              textAlign: TextAlign.center,
            )),
        new Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              width: 5.0,
            ),
            new FlatButton(
              padding: EdgeInsets.all(0.0),
              onPressed: () {
                Navigator.push(
                    context,
                    new MaterialPageRoute(
                      builder: (context) => new MseHome(),
                    ));
              },
              child: new CircleAvatar(
                  radius: 60.0,
                  foregroundColor: Theme.of(context).primaryColor,
                  backgroundColor: Colors.green[900],
                  child: new Text(
                    "Assessment",
                    style: new TextStyle(
                        color: Colors.orange,
                        fontSize: 18.0,
                        fontWeight: FontWeight.w500),
                  )),
            ),
            SizedBox(
              width: 20.0,
            ),
            new FlatButton(
              padding: EdgeInsets.all(0.0),
              onPressed: () {
                Navigator.push(
                    context,
                    new MaterialPageRoute(
                      builder: (context) => AlBooks('data_json/books.json'),
                    ));
              },
              child: new CircleAvatar(
                  radius: 60.0,
                  foregroundColor: Theme.of(context).primaryColor,
                  backgroundColor: Colors.green[900],
                  child: new Text(
                    "Farmers' Info",
                    style: new TextStyle(
                        color: Colors.orange,
                        fontSize: 18.0,
                        fontWeight: FontWeight.w500),
                  )),
            ),
            SizedBox(width: 20.0),
            new CircleAvatar(
                radius: 60.0,
                foregroundColor: Theme.of(context).primaryColor,
                backgroundColor: Colors.grey,
                child: new Text(
                  "News",
                  style: new TextStyle(
                      color: Colors.white,
                      fontSize: 18.0,
                      fontWeight: FontWeight.w500),
                )),
          ],
        ),
        SizedBox(height: 50.0),
        Card(
            color: Colors.grey,
            child: Column(
              children: <Widget>[
                Container(
                    width: 450.0,
                    height: 200.0,
                    padding: const EdgeInsets.only(
                        left: 15.0, top: 10.0, bottom: 10.0, right: 15.0),
                    alignment: new FractionalOffset(0.0, 0.0),
                    child: new Column(
                      children: <Widget>[
                        Text("Notice Board",
                            style: const TextStyle(
                                fontStyle: FontStyle.normal,
                                fontSize: 17.0,
                                color: Colors.white,
                                fontWeight: FontWeight.w600),
                            textAlign: TextAlign.center),
                            SizedBox(height: 30.0,),
                            Text("Please note that next assesments will me on this date 00-00-000", style: TextStyle(color:Colors.white),)
                      ],
                    ))
              ],
            )),
      ],
    );
  }
}
