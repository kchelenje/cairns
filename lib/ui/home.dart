import 'package:flutter/material.dart';
import 'package:Libresmit/ui/screens/practice_screen.dart';
import 'package:Libresmit/ui/screens/topical_screen.dart';

class MseHome extends StatefulWidget {
  MseHome();

  @override
  _MseHomeState createState() => new _MseHomeState();
}

class _MseHomeState extends State<MseHome> with SingleTickerProviderStateMixin {
  TabController _tabController;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, initialIndex: 0, length: 2);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKey,
      drawer: Drawer(
        child: new ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            new DrawerHeader(
              child: new Text(
                'Cairns Food',
                style: new TextStyle(
                    color: Colors.white,
                    fontFamily: 'sans-serif',
                    fontSize: 22.0),
              ),
              decoration: new BoxDecoration(
                color: Colors.green[900],
                borderRadius: BorderRadius.circular(8.5),
              ),
            ),
            new ListTile(
              leading: Icon(Icons.info, size: 27.0, color: Colors.orangeAccent),
              title: new Text('About App'),
              onTap: () {
                showAboutDialog(
                  context: context,
                  applicationVersion: "1.0",
                );
              },
            ),
            new ListTile(
              leading:
                  Icon(Icons.person, color: Colors.orangeAccent, size: 27.0),
              title: new Text('Developer Info'),
              onTap: () {
                _showDeveloperDialog();
              },
            ),
          ],
        ),
      ),
      appBar: new AppBar(
        backgroundColor: Colors.green[900],
        leading: new IconButton(
            icon: new Icon(Icons.more_vert),
            onPressed: () => _scaffoldKey.currentState.openDrawer()),
        title: new Text("Cairns Farmers Data Matter"),
        centerTitle: true,
        elevation: 2.0,
        bottom: new TabBar(
          controller: _tabController,
          indicatorColor: Colors.orange,
          tabs: <Widget>[
            new Tab(
              text: "Crop Assessment",
            ),
            new Tab(text: "Grower Assesssmentr"),

          ],
        ),
        actions: <Widget>[
          new Icon(Icons.search),
          new Padding(
            padding: const EdgeInsets.symmetric(horizontal: 5.0),
          ),
        ],
      ),
      body: new TabBarView(
        controller: _tabController,
        children: <Widget>[
          new TopicalScreen('data_json/topics_json.json'),
          new PracticeScreen('data_json/exam_json.json'),
        ],
      ),
    );
  }

  void showAboutDialog({
    @required BuildContext context,
    String applicationName,
    String applicationVersion,
    Widget applicationIcon,
    String applicationLegalese,
    List<Widget> children,
  }) {
    assert(context != null);
    showDialog<void>(
        context: context,
        builder: (BuildContext context) {
          return AboutDialog(
            applicationName: applicationName,
            applicationVersion: applicationVersion,
            applicationIcon: applicationIcon,
            applicationLegalese: applicationLegalese,
            children: children,
          );
        });
  }

  void _showDeveloperDialog() {
    var alert = new AlertDialog(
      content: new Container(
          child: new Text(
        "Developed by:\n\n Global Network Point\n\n For:\n\n Cairns Foods",
        style:
            new TextStyle(color: Colors.green[900], fontWeight: FontWeight.w500),
        textAlign: TextAlign.center,
      )),
      actions: <Widget>[
        new FlatButton(
            onPressed: () => Navigator.pop(context), child: Text("Close"))
      ],
    );

    showDialog(
        context: context,
        builder: (_) {
          return alert;
        });
  }
}
