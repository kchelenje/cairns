import 'package:flutter/material.dart';

class TopicalScreen extends StatefulWidget {
  final String listLink;
  TopicalScreen(this.listLink);
  @override
  _TopicalScreenSate createState() =>
      new _TopicalScreenSate(dataLink: new TopicalScreen(listLink));
}

class _TopicalScreenSate extends State<TopicalScreen> {
  final TopicalScreen dataLink;
  _TopicalScreenSate({Key key, @required this.dataLink});

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            image: new DecorationImage(
                fit: BoxFit.cover,
                colorFilter: new ColorFilter.mode(
                    Colors.black.withOpacity(0.7), BlendMode.dstATop),
                image: new AssetImage("images/background.jpg"))),
        child: Card(
          color: Colors.white70,
          child: Container(
            margin: EdgeInsets.only(left: 15.0, right: 15.0, top: 20.0),
            child: Column(
              children: <Widget>[
                Text("CROP ASSESSMENT"),
                SizedBox(height: 30.0),
                TextField(
                  decoration: InputDecoration(
                    hintText: 'Enter Farmer Name',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: BorderSide(
                        width: 0.1,
                        color: Colors.red,
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 10.0),
                TextField(
                  decoration: InputDecoration(
                    hintText: 'Enter Farmer Name',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: BorderSide(
                        width: 0.1,
                        color: Colors.red,
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 10.0),
                TextField(
                  decoration: InputDecoration(
                    hintText: 'Enter Farmer Name',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: BorderSide(
                        width: 0.1,
                        color: Colors.red,
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 10.0),
                TextField(
                  decoration: InputDecoration(
                    hintText: 'Enter Farmer Name',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: BorderSide(
                        width: 0.1,
                        color: Colors.red,
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 10.0),
                TextField(
                  decoration: InputDecoration(
                    hintText: 'Enter Farmer Name',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: BorderSide(
                        width: 0.1,
                        color: Colors.red,
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 10.0),
                TextField(
                  decoration: InputDecoration(
                    hintText: 'Enter Farmer Name',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: BorderSide(
                        width: 0.1,
                        color: Colors.red,
                      ),
                    ),
                  ),
                ),
                RaisedButton(
                  onPressed: () => print("submited"),
                  child: Text("Submit Assesment"),
                )
              ],
            ),
          ),
        ));
  }
}
