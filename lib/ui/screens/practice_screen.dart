import 'package:flutter/material.dart';


class PracticeScreen extends StatefulWidget {
  final String listLink;
  PracticeScreen(this.listLink);
  @override
  _PracticeScreenSate createState() =>
      new _PracticeScreenSate(dataLink: new PracticeScreen(listLink));
}

class _PracticeScreenSate extends State<PracticeScreen> {
  final PracticeScreen dataLink;
  _PracticeScreenSate({Key key, @required this.dataLink});
  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            image: new DecorationImage(
                fit: BoxFit.cover,
                colorFilter: new ColorFilter.mode(
                    Colors.black.withOpacity(0.7), BlendMode.dstATop),
                image: new AssetImage("images/background.jpg"))),
        child: Card(
          color: Colors.white70,
          child: Container(
            margin: EdgeInsets.only(left: 15.0, right: 15.0, top: 20.0),
            child: Column(
              children: <Widget>[
                Text("GROWER ASSESSMENT"),
                SizedBox(height: 30.0),
                TextField(
                  decoration: InputDecoration(
                    hintText: 'Enter Farmer Name',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: BorderSide(
                        width: 0.1,
                        color: Colors.red,
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 10.0),
                TextField(
                  decoration: InputDecoration(
                    hintText: 'Enter Farmer Name',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: BorderSide(
                        width: 0.1,
                        color: Colors.red,
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 10.0),
                TextField(
                  decoration: InputDecoration(
                    hintText: 'Enter Farmer Name',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: BorderSide(
                        width: 0.1,
                        color: Colors.red,
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 10.0),
                TextField(
                  decoration: InputDecoration(
                    hintText: 'Enter Farmer Name',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: BorderSide(
                        width: 0.1,
                        color: Colors.red,
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 10.0),
                TextField(
                  decoration: InputDecoration(
                    hintText: 'Enter Farmer Name',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: BorderSide(
                        width: 0.1,
                        color: Colors.red,
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 10.0),
                TextField(
                  decoration: InputDecoration(
                    hintText: 'Enter Farmer Name',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: BorderSide(
                        width: 0.1,
                        color: Colors.red,
                      ),
                    ),
                  ),
                ),
                RaisedButton(
                  onPressed: () => print("submited"),
                  child: Text("Submit Assesment"),
                )
              ],
            ),
          ),
        ));
  }
}
