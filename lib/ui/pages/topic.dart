import 'package:flutter/material.dart';
import 'dart:convert';

class Topic1Screen extends StatefulWidget {
  final String contentLink;
  final String topicName;
  Topic1Screen(this.contentLink, this.topicName);

  @override
  _Topic1ScreenState createState() => new _Topic1ScreenState(
      dataLink: new Topic1Screen(contentLink, topicName));
}

class _Topic1ScreenState extends State<Topic1Screen>
    with SingleTickerProviderStateMixin {
  @override
  void initState() {
    super.initState();
  }

  final Topic1Screen dataLink;
  _Topic1ScreenState({Key key, @required this.dataLink});

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text(
            '${dataLink.topicName}',
            style: new TextStyle(fontFamily: 'mse-app-bars', fontSize: 18.0),
          ),
          backgroundColor: Colors.green[900],
          centerTitle: true,
        ),
        body: new Container(
            decoration: BoxDecoration(
                image: new DecorationImage(
                    fit: BoxFit.cover,
                    colorFilter: new ColorFilter.mode(
                        Colors.black.withOpacity(0.4), BlendMode.dstATop),
                    image: new AssetImage("images/back.jpg"))),
            child: new FutureBuilder(
                future: DefaultAssetBundle.of(context)
                    .loadString('${dataLink.contentLink}'),
                builder: (context, snapshot) {
                  var topicData = json.decode(snapshot.data.toString());
                  return new ListView.builder(
                    itemCount: topicData == null ? 0 : topicData.length,
                    itemBuilder: (BuildContext context, int index) =>
                        new Column(
                          children: [
                            topicData[index]['subtopic'] != null
                                ? topic(topicData, index)
                                : Row(),
                            new Container(
                              padding: const EdgeInsets.only(
                                  left: 10.0, right: 10.0),
                              child: new Card(
                                child: new Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    topicData[index]['paragraph']
                                                ['explanation'] !=
                                            null
                                        ? explanation(topicData, index)
                                        : Row(),
                                    topicData[index]['paragraph']
                                                ['experiment'] !=
                                            null
                                        ? experiment(topicData, index)
                                        : Row(),
                                    topicData[index]['paragraph']
                                                ['application'] !=
                                            null
                                        ? application(topicData, index)
                                        : Row(),
                                    topicData[index]['paragraph']['picture'] !=
                                            null
                                        ? displayImage(topicData, index)
                                        : Row(),
                                    topicData[index]['paragraph']['picture2'] !=
                                            null
                                        ? displayImage2(topicData, index)
                                        : Row(),
                                    topicData[index]['paragraph']['solution'] !=
                                            null
                                        ? examSol(topicData, index)
                                        : Row(),
                                    SizedBox(
                                      height: 30.0,
                                    ),
                                    Text(
                                      "View Crop Assessment Info",
                                      textAlign: TextAlign.center,
                                    ),
                                    SizedBox(
                                      height: 30.0,
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                  );
                })));
  }

  Widget topic(var topicData, int index) {
    return new Container(
      padding: const EdgeInsets.only(left: 10.0, right: 10.0, top: 5.0),
      child: new Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          new Text(
            topicData[index]['subtopic'],
            textAlign: TextAlign.center,
            overflow: TextOverflow.ellipsis,
            style: new TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 18.0,
                color: Colors.orange),
          ),
        ],
      ),
    );
  }

  Widget explanation(var topicData, int index) {
    return new Container(
      padding: const EdgeInsets.only(
          left: 15.0, top: 10.0, bottom: 10.0, right: 15.0),
      alignment: new FractionalOffset(0.0, 0.0),
      child: new Text(topicData[index]['paragraph']['explanation'],
          style: const TextStyle(fontStyle: FontStyle.normal, fontSize: 17.0),
          textAlign: topicData[index]['paragraph']['formula'] == 'true'
              ? TextAlign.center
              : TextAlign.justify),
    );
  }

  Widget application(var topicData, int index) {
    return new Column(
      children: <Widget>[
        Container(child: Image.asset("images/background.jpg")),
        Container(
          decoration:
              BoxDecoration(shape: BoxShape.circle, color: Colors.black12),
          padding: const EdgeInsets.only(
              left: 15.0, top: 10.0, bottom: 10.0, right: 15.0),
          alignment: new FractionalOffset(0.0, 0.0),
          //color: Colors.amber,
          child: new Text(topicData[index]['paragraph']['application'],
              style:
                  const TextStyle(fontStyle: FontStyle.normal, fontSize: 17.0),
              textAlign: topicData[index]['paragraph']['formula'] == 'true'
                  ? TextAlign.center
                  : TextAlign.justify),
        )
      ],
    );
  }

  Widget experiment(var topicData, int index) {
    return new Container(
      padding: const EdgeInsets.only(
          left: 15.0, top: 10.0, bottom: 10.0, right: 15.0),
      alignment: new FractionalOffset(0.0, 0.0),
      color: Colors.green[100],
      child: new Column(
        children: <Widget>[
          Stack(
            children: <Widget>[
              Container(
                  width: 100.0,
                  // margin: EdgeInsets.only(left:10.0),
                  child: ListTile(
                      leading: Icon(
                    Icons.equalizer,
                    color: Colors.green[900],
                    size: 40.0,
                  ))),
              new Container(
                  width: 250.0,
                  margin: EdgeInsets.only(left: 60.0),
                  child: new Text("Experiment",
                      style: const TextStyle(
                          color: Colors.black26, fontSize: 33.0))),
              Container(
                  width: 100.0,
                  margin: EdgeInsets.only(left: 220.0),
                  child: ListTile(
                      leading: Icon(
                    Icons.equalizer,
                    color: Colors.green[900],
                    size: 40.0,
                  ))),
            ],
          ),
          new Text(topicData[index]['paragraph']['experiment'],
              style:
                  const TextStyle(fontStyle: FontStyle.italic, fontSize: 15.0),
              textAlign: topicData[index]['paragraph']['formula'] == 'true'
                  ? TextAlign.center
                  : TextAlign.justify),
        ],
      ),
    );
  }

  Widget displayImage(var topicData, int index) {
    return new Container(
        child: new Image.asset(
      topicData[index]['paragraph']['picture'],
      fit: BoxFit.contain,
    ));
  }

  Widget displayImage2(var topicData, int index) {
    return new Container(
        child: new Image.asset(
      topicData[index]['paragraph']['picture2'],
      fit: BoxFit.contain,
    ));
  }

  Widget examSol(var topicData, int index) {
    return new Container(
        decoration: BoxDecoration(
          gradient: RadialGradient(
              colors: [Colors.green[900], Colors.lightGreen, Colors.green[700]],
              center: Alignment.center,
              radius: 1.4,
              focalRadius: 0.5,
              stops: [0.0, 0.3, 0.7],
              tileMode: TileMode.clamp),
        ),
        padding: const EdgeInsets.only(top: 5.0),
        child: Column(children: <Widget>[
          new Container(
            margin: EdgeInsets.only(left: 25.0, right: 25.0),
            padding: const EdgeInsets.only(
                left: 10.0, right: 10.0, top: 5.0, bottom: 8.0),
            color: Colors.white54,
            child: new Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                new Text(
                  topicData[index]['paragraph']['example'],
                  textAlign: TextAlign.justify,
                  style: new TextStyle(
                      fontStyle: FontStyle.italic,
                      fontSize: 15.0,
                      color: Colors.black,
                      fontFamily: 'cursive'), //merienda-regular
                ),
              ],
            ),
          ),
          new Text(
            "\n Solution \n",
            textAlign: TextAlign.center,
            overflow: TextOverflow.ellipsis,
            style: new TextStyle(
                fontStyle: FontStyle.italic,
                fontWeight: FontWeight.w600,
                fontSize: 16.0,
                color: Colors.white),
          ),
          Container(
            padding: const EdgeInsets.only(
                left: 10.0, right: 10.0, top: 5.0, bottom: 8.0),
            color: Colors.orangeAccent,
            margin: EdgeInsets.only(left: 25.0, right: 25.0, bottom: 8.0),
            child: new Text(topicData[index]['paragraph']['solution'],
                style: TextStyle(
                    fontStyle: FontStyle.normal,
                    fontSize: 12.0,
                    color: Colors.green[900],
                    fontFamily: 'mse-app-solution'),
                textAlign: TextAlign.center),
          ),
        ]));
  }
}
