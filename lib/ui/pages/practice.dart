import 'package:flutter/material.dart';

class Practice extends StatefulWidget {
  final String practiceLink;
  Practice(this.practiceLink);

  @override
  PracticeState createState() =>
      PracticeState(dataLink: new Practice(practiceLink));
}

class PracticeState extends State<Practice> with TickerProviderStateMixin {
  final Practice dataLink;
  var timeleft;
  PracticeState({Key key, @required this.dataLink});

  AnimationController controller;

  String get timerString {
    Duration duration = controller.duration * controller.value;
    if ('${(duration.inHours % 60).toString().padLeft(2, '0')}:${(duration.inMinutes % 60).toString().padLeft(2, '0')}:${(duration.inSeconds % 60).toString().padLeft(2, '0')}' ==
        "01:28:00") {
      timeleft =
          '${(duration.inHours % 60).toString().padLeft(2, '0')}:${(duration.inMinutes % 60).toString().padLeft(2, '0')}:${(duration.inSeconds % 60).toString().padLeft(2, '0')}';
      return '${(duration.inHours % 60).toString().padLeft(2, '0')}:${(duration.inMinutes % 60).toString().padLeft(2, '0')}:${(duration.inSeconds % 60).toString().padLeft(2, '0')}';
    } else {
      return '${(duration.inHours % 60).toString().padLeft(2, '0')}:${(duration.inMinutes % 60).toString().padLeft(2, '0')}:${(duration.inSeconds % 60).toString().padLeft(2, '0')}';
    }
  }

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
      vsync: this,
      duration: Duration(hours: 1, minutes: 30),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: new AppBar(
          title: new Text('${dataLink.practiceLink}'),
          centerTitle: true,
          backgroundColor: Colors.green[900],
        ),
        body: new Container(
          decoration: BoxDecoration(
              image: new DecorationImage(
                  fit: BoxFit.cover,
                  colorFilter: new ColorFilter.mode(
                      Colors.black.withOpacity(0.3), BlendMode.dstATop),
                  image: new AssetImage("images/background.jpg"))),
          child: new Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new Container(
                  padding: EdgeInsets.only(left: 120.0),
                  color: timeleft == "01:28:00"
                      ? Colors.orangeAccent
                      : Colors.amber,
                  child: new Row(
                    children: <Widget>[
                      IconButton(
                        color: Colors.green[900],
                        iconSize: 35.0,
                        icon: Icon(Icons.play_arrow),
                        tooltip: 'Start Practice Questions',
                        onPressed: () {
                          if (controller.isAnimating)
                            controller.stop();
                          else {
                            controller.reverse(
                                from: controller.value == 0.0
                                    ? 1.0
                                    : controller.value);
                          }
                        },
                      ),
                      new Container(
                        child: AnimatedBuilder(
                            animation: controller,
                            builder: (BuildContext context, Widget child) {
                              return new Text(
                                timerString,
                                style: new TextStyle(fontSize: 20.0),
                              );
                            }),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ));
  }
}
