import 'package:flutter/material.dart';
import 'package:Libresmit/ui/home.dart';
import 'package:Libresmit/ui/pages/topic.dart';


class AlBooks extends StatefulWidget {
  final String contentLink;

  AlBooks(this.contentLink);

  @override
  _AlBooksState createState() =>
    new _AlBooksState(dataLink: new AlBooks(contentLink));
}

class _AlBooksState extends State<AlBooks> with SingleTickerProviderStateMixin {
  @override
  void initState() {
    super.initState();
  }

  final AlBooks dataLink;
  _AlBooksState({Key key, @required this.dataLink});

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text(
            'Cairnes Foods',
            style: new TextStyle(fontFamily: 'mse-app-bars', fontSize: 18.0),
          ),
          backgroundColor: Colors.green[900],
          centerTitle: true,
        ),
        body: new Container(
            decoration: BoxDecoration(
                image: new DecorationImage(
                    fit: BoxFit.cover,
                    colorFilter: new ColorFilter.mode(
                        Colors.black.withOpacity(0.7), BlendMode.dstATop),
                    image: new AssetImage("images/background.jpg"))),
            child: new FutureBuilder(
                future: DefaultAssetBundle.of(context)
                    .loadString('${dataLink.contentLink}'),
                builder: (context, snapshot) {
                  //  var topicData = json.decode(snapshot.data.toString());
                  return new Container(
                    
                      padding: EdgeInsets.only(top: 20.0),
                      child: ListView(children: <Widget>[
                        new Column(
                          children: <Widget>[application()],
                        ),
                      ]));
                })));
  }

  Widget application() {
    return new Column(
      children: <Widget>[
        Container(
            margin: EdgeInsets.only(bottom: 20.0, top: 25.0),
            child: new Text(
              "Welcome to Cairnes Farmers' Info Portal (Agent)",
              style: TextStyle(
                  fontSize: 25.0,
                  fontWeight: FontWeight.w900,
                  fontFamily: "mse-app-bars",
                  color: Colors.green[900]),
              textAlign: TextAlign.center,
            )),
        new Row(
           mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            new FlatButton(
              padding: EdgeInsets.all(0.0),
              onPressed: () {
                Navigator.push(
                    context,
                    new MaterialPageRoute(
                     // builder: (context) => new MseHome(),
                     builder: (context) => Topic1Screen('data_json/chapter1.json','Farmer 1 Assessment Info')
                    ));
              },
              child: Card(
                  elevation: 15.0,
                  color: Colors.green[900],
                  child: Column(
                    children: <Widget>[
                      Container(
                          width: 127.0,
                          child: Image.asset("images/matter.png")),
                      Container(
                        width: 127.0,
                        padding: const EdgeInsets.only(
                            left: 15.0, top: 10.0, bottom: 10.0, right: 15.0),
                        alignment: new FractionalOffset(0.0, 0.0),
                        child: new Text("Dummy Farmer 1",
                            style: const TextStyle(
                                fontStyle: FontStyle.normal,
                                fontSize: 17.0,
                                color: Colors.orangeAccent,
                                fontWeight: FontWeight.w600),
                            textAlign: TextAlign.center),
                      )
                    ],
                  )),
            ),
            Card(
                color: Colors.grey,
                child: Column(
                  children: <Widget>[
                    Container(
                        width: 127.0,
                        child: Image.asset(
                          "images/matter.png",
                          color: Colors.white24,
                        )),
                    Container(
                      width: 127.0,
                      padding: const EdgeInsets.only(
                          left: 15.0, top: 10.0, bottom: 10.0, right: 15.0),
                      alignment: new FractionalOffset(0.0, 0.0),
                      child: new Text("Dummy Farmer 2",
                          style: const TextStyle(
                              fontStyle: FontStyle.normal,
                              fontSize: 17.0,
                              color: Colors.white,
                              fontWeight: FontWeight.w600),
                          textAlign: TextAlign.center),
                    )
                  ],
                )),
            Card(
                color: Colors.grey,
                child: Column(
                  children: <Widget>[
                    Container(
                        width: 127.0,
                        child: Image.asset(
                          "images/matter.png",
                          color: Colors.white24,
                        )),
                    Container(
                      width: 127.0,
                      padding: const EdgeInsets.only(
                          left: 15.0, top: 10.0, bottom: 10.0, right: 15.0),
                      alignment: new FractionalOffset(0.0, 0.0),
                      child: new Text("Dummy Farmer 3",
                          style: const TextStyle(
                              fontStyle: FontStyle.normal,
                              fontSize: 17.0,
                              color: Colors.white,
                              fontWeight: FontWeight.w600),
                          textAlign: TextAlign.center),
                    )
                  ],
                )),
          ],
        ),
        new Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Card(
                color: Colors.grey,
                child: Column(
                  children: <Widget>[
                    Container(
                        width: 127.0,
                        child: Image.asset("images/matter.png",
                            color: Colors.white24)),
                    Container(
                      width: 127.0,
                      padding: const EdgeInsets.only(
                          left: 15.0, top: 10.0, bottom: 10.0, right: 15.0),
                      alignment: new FractionalOffset(0.0, 0.0),
                      child: new Text("Dummy Farmer 4",
                          style: const TextStyle(
                              fontStyle: FontStyle.normal,
                              fontSize: 17.0,
                              color: Colors.white,
                              fontWeight: FontWeight.w600),
                          textAlign: TextAlign.center),
                    )
                  ],
                )),
            Card(
                color: Colors.grey,
                child: Column(
                  children: <Widget>[
                    Container(
                        width: 127.0,
                        child: Image.asset(
                          "images/matter.png",
                          color: Colors.white24,
                        )),
                    Container(
                      width: 127.0,
                      padding: const EdgeInsets.only(
                          left: 15.0, top: 10.0, bottom: 10.0, right: 15.0),
                      alignment: new FractionalOffset(0.0, 0.0),
                      child: new Text("Dummy Farmer 5",
                          style: const TextStyle(
                              fontStyle: FontStyle.normal,
                              fontSize: 17.0,
                              color: Colors.white,
                              fontWeight: FontWeight.w600),
                          textAlign: TextAlign.center),
                    )
                  ],
                )),
            Card(
                color: Colors.grey,
                child: Column(
                  children: <Widget>[
                    Container(
                        width: 127.0,
                        child: Image.asset(
                          "images/matter.png",
                          color: Colors.white24,
                        )),
                    Container(
                      width: 127.0,
                      padding: const EdgeInsets.only(
                          left: 15.0, top: 10.0, bottom: 10.0, right: 15.0),
                      alignment: new FractionalOffset(0.0, 0.0),
                      child: new Text("Dummy Farmer 6",
                          style: const TextStyle(
                              fontStyle: FontStyle.normal,
                              fontSize: 17.0,
                              color: Colors.white,
                              fontWeight: FontWeight.w600),
                          textAlign: TextAlign.center),
                    )
                  ],
                )),
          ],
        ),
      ],
    );
  }
}
