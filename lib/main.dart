import 'package:flutter/material.dart';
import 'package:Libresmit/ui/al_books.dart';
import 'package:Libresmit/ui/landing.dart';


void main() {
  runApp(
    new MaterialApp(
    home: MyApp(),
    theme: ThemeData(fontFamily: 'sans-serif'),
    debugShowCheckedModeBanner: true,));
}
class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return new  StartApp();  }
}

class StartApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
    //  body:  new AlBooks('data_json/books.json'),
    body: new Landing('data_json/books.json'),
    );
  }
}